/* global fetch */
class API {
  constructor() {
    this.roomsURL = 'http://devcollege_react.devmerix.com/api/0/rooms';
    this.messagesURL = 'http://devcollege_react.devmerix.com/api/0/messages';
  }
    
  fetch(method, url, options = {}){
    const requestOptions = {
        ...options,
        method: method,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    };
    return fetch(url, requestOptions)
      .then(res => res.json())
  }
  
  fetchMessages() {
    return this.fetch('GET', this.messagesURL);
  }      
  
  fetchRooms() {
    return this.fetch('GET', this.roomsURL);
  }    
  
  createNewRoom(options) {
    return this.fetch('POST', this.roomsURL, options);
  }
}

export default new API();
