import store from '../store';

import wsClient from '../ws-client';

import { MESSAGE_ADD, REQUEST_MESSAGES, RECEIVE_MESSAGES } from '../constants/MessageActionTypes';
import API from '../API';

export function newMessage(message) {
  return {
    type: MESSAGE_ADD,
    id: message.id,
    text: message.text,
    roomId: message.roomId,
    from: message.from,
    createdAt: message.createdAt
  };
}

export function fetchMessages() {
  return dispatch => {
    dispatch(requestMessages());
    return API.fetchMessages()
      .then(json => dispatch(receiveMessages(json)));
  }
}

function requestMessages() {
  return {
    type: REQUEST_MESSAGES
  };
}

function receiveMessages(messages) {
  return {
    type: RECEIVE_MESSAGES,
    messages: messages
  };
}

wsClient.on('message:added', (data) => {
  store.dispatch(newMessage(data.new_val));
});
