import { USER_CHANGE_NAME, USER_JOIN_ROOM } from '../constants/UserActionTypes';

export function changeName(name) {
  return {
    type: USER_CHANGE_NAME,
    name
  };
}

export function joinRoom(id) {
  return {
    type: USER_JOIN_ROOM,
    id
  };
}
