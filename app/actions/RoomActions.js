import store from '../store';

import wsClient from '../ws-client';

import { ROOM_ADD, ROOM_UPDATE, ROOM_DELETE, ROOM_JOIN, REQUEST_ROOMS, RECEIVE_ROOMS } from '../constants/RoomActionTypes';
import API from '../API';

export function addRoom(room) {
  return {
    type: ROOM_ADD,
    room
  };
}

export function updateRoom(room) {
  return {
    type: ROOM_UPDATE,
    room
  };
}

export function deleteRoom(room) {
  return {
    type: ROOM_DELETE,
    room
  };
}

export function joinRoom(index) {
  return {
    type: ROOM_JOIN,
    index
  };
}

export function createRoom(room) {
  return () => {
    return API.createNewRoom({
      body: JSON.stringify({
        name: room.name
      })
    });
  };
}

export function fetchRooms() {
  return dispatch => {
    dispatch(requestRooms());
    return API.fetchRooms()
      .then(json => dispatch(receiveRooms(json)));
  };
}

function requestRooms() {
  return {
    type: REQUEST_ROOMS
  };
}

function receiveRooms(rooms) {
  return {
    type: RECEIVE_ROOMS,
    rooms
  };
}

wsClient.on('rooms:added', (data) => {
  store.dispatch(addRoom(data.new_val));
});

wsClient.on('rooms:updated', (data) => {
  store.dispatch(updateRoom(data.new_val));
});

wsClient.on('rooms:deleted', (data) => {
  store.dispatch(deleteRoom(data.old_val));
});
