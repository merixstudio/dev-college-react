import { combineReducers } from 'redux';
import { rooms } from './rooms';
import { messages } from './messages';
import { user } from './user';

const rootReducer = combineReducers({
  rooms,
  messages,
  user
});

export default rootReducer;
