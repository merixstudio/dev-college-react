import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import Drawer from 'material-ui/Drawer';
import {List, ListItem} from 'material-ui/List';
import IconMenu from 'material-ui/IconMenu';
import Subheader from 'material-ui/Subheader';
import MenuItem from 'material-ui/MenuItem';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import { orange500 } from 'material-ui/styles/colors';

import NavigationClose from 'material-ui/svg-icons/navigation/close';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

import { withRouter } from 'react-router-dom';
import * as RoomActions from '../actions/RoomActions';
import * as MessageActions from '../actions/MessageActions';
import * as UserActions from '../actions/UserActions';

class ChatContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sideNavOpen: false,
      modalOpen: false
    };
  }

  componentDidMount() {
    this.props.roomActions.fetchRooms();
    this.props.messageActions.fetchMessages();
  }

  toggleNav() {
    this.setState({ sideNavOpen: !this.state.sideNavOpen });
  }

  handleModalOpen() {
    this.setState({modalOpen: true});
  }

  handleModalClose() {
    this.setState({modalOpen: false});
  }

  handleRoomCreate() {
    this.props.roomActions.createRoom({ name: this.roomName.getValue() });
    this.handleModalClose();
  }

  handleUserNameChange() {
    this.props.userActions.changeName(this.userName.getValue());
  }

  onRoomClick(room) {
    this.props.history.push('/channel/' + room.id);
    this.props.userActions.joinRoom(room.id);

    this.setState({ sideNavOpen: false });
  }

  goToLobby() {
    this.props.history.push('/');
  }

  render() {
    const { children, rooms } = this.props;

    const rightIconMenu = (
      <IconMenu iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}>
        <MenuItem>Change Name</MenuItem>
        <MenuItem>Delete</MenuItem>
      </IconMenu>
    );

    const roomsItems = rooms.list.map((room) => {
      return (
        <ListItem primaryText={room.name} rightIconButton={rightIconMenu} key={room.id} onClick={() => this.onRoomClick(room)}/>
      );
    });

    const actions = [
      <FlatButton
        label="Submit"
        primary={true}
        onClick={() => this.handleRoomCreate()} />,
      <FlatButton
        label="Cancel"
        secondary={true}
        onClick={() => this.handleModalClose()} />
    ];

    const userInputStyles = {
      underlineStyle: {
        borderColor: orange500
      },
      text: {
        color: '#fff'
      }
    };

    return (
      <MuiThemeProvider>
        <div className="container">
          <AppBar
            style={{minHeight: 64}}
            title="DevCollege React/Redux Chat Application"
            titleStyle={{cursor: 'pointer'}}
            onLeftIconButtonTouchTap={() => this.toggleNav()}
            onTitleTouchTap={() => this.goToLobby()}
            iconElementRight={<TextField name="username" ref={userName => this.userName = userName} defaultValue={this.props.user.name} underlineStyle={userInputStyles.underlineStyle} underlineFocusStyle={userInputStyles.underlineStyle} inputStyle={userInputStyles.text} onBlur={() => this.handleUserNameChange()} />} />
          {children}
          <Drawer open={this.state.sideNavOpen} width={400} docked={false}>
            <AppBar title="Rooms List"
              iconElementLeft={<IconButton onClick={() => this.toggleNav()}><NavigationClose /></IconButton>}
              iconElementRight={<FlatButton label="Add new room" onClick={() => this.handleModalOpen()} />} />
            <List>
            <Subheader>Rooms</Subheader>
              {(rooms.list.length > 0) ? roomsItems : <ListItem disabled={true} primaryText="No rooms" />}
            </List>
          </Drawer>
          <Dialog
            title="Name new room"
            actions={actions}
            modal={false}
            open={this.state.modalOpen}
            onRequestClose={() => this.handleModalClose()}>
            <TextField
              name="roomname"
              ref={(roomName) => this.roomName = roomName}
              hintText="Enter Room Name"
              floatingLabelText="Room name"
              defaultValue=""
              fullWidth={true} />
          </Dialog>
        </div>
      </MuiThemeProvider>
    );
  }
}

function mapStateToProps(state) {
  return {
    routing: state.routing,
    messages: state.messages,
    rooms: state.rooms,
    user: state.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    roomActions: bindActionCreators(RoomActions, dispatch),
    messageActions: bindActionCreators(MessageActions, dispatch),
    userActions: bindActionCreators(UserActions, dispatch)
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ChatContainer));
