import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';

import MessageList from '../components/MessageList';
import MessageInput from '../components/MessageInput';
import * as UserActions from '../actions/UserActions';

import wsClient from '../ws-client';

class RoomContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    if(!this.props.user.curentRoom) {
      this.props.userActions.joinRoom(this.props.match.params.roomId);
    }
  }

 componentDidUpdate(prevProps) {
  if(prevProps.messages.items.length + 1 === this.props.messages.items.length) {
    const el = document.querySelector('.overflow-container');
    el.scrollTop = el.children[0].scrollHeight;
  }
 }

  sendMessage(text) {
    const { user } = this.props;

    wsClient.send('message', { text: text, from: user.name, roomId: user.currentRoom });
  }

  getRoomMessages() {
    const { messages } = this.props;
    const { params } = this.props.match;

    return messages.items.filter((message) => {
      return message.roomId === params.roomId
    });
  }

  render() {
    const messages = this.getRoomMessages();

    return (
      <div className="overflow-container" style={{overflowY: 'auto', marginBottom: '58px'}}>
        <MessageList messages={messages} />
        <MessageInput onSend={(text) => this.sendMessage(text)} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    messages: state.messages,
    rooms: state.rooms,
    user: state.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    userActions: bindActionCreators(UserActions, dispatch)
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RoomContainer));
