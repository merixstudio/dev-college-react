import React from 'react';
import { connect } from 'react-redux';

class LobbyContainer extends React.Component {
  render() {
    return (
      <div className="lobby">
        <h2 className="intro-text">Open menu on the left to join or create room</h2>
      </div>
    );
  }
}

function select(state) {
  return {
    messages: state.messages,
    rooms: state.rooms,
    user: state.user
  };
}

export default connect(select)(LobbyContainer);
