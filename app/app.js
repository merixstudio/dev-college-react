import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route } from 'react-router-dom';

import store from './store';

import ChatContainer from './containers/ChatContainer';
import LobbyContainer from './containers/LobbyContainer';
import RoomContainer from './containers/RoomContainer';

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <ChatContainer>
            <Route exact path="/" component={LobbyContainer} />
            <Route path="/channel/:roomId" component={RoomContainer} />
          </ChatContainer>
        </BrowserRouter>
      </Provider>
    );
  }
}
